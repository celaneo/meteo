import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/home";
function App() {
  return (
    <div className='App'>
      <h1 className='title'>weather</h1>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='*' element={<h1>404</h1>} />
      </Routes>
    </div>
  );
}

export default App;
