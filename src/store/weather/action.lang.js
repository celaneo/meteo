import * as t from "./actionTypes";
import axios from "axios";
import { uiStartLoading, uiStopLoading } from "../loading/action.ui";
export const getWeather = (payload) => {
  const config = {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  };
  return (dispatch) => {
    dispatch(uiStartLoading());
    axios
      .get(
        process.env.REACT_APP_BASE_URL +
          `?q=${payload.city}&appid=f825344b0cf0672c689378549f9868db&units=metric`,
        config
      )
      .then(function (response) {
        dispatch(setWeather(response.data));

        dispatch(uiStopLoading());
      })
      .catch(function (error) {
        dispatch(setWeatherError(error.response.data));
        dispatch(uiStopLoading());
        console.log(error);
      });
  };
};

export const setWeather = (value) => {
  return {
    type: t.WEATHER_LIST,
    weather: value,
  };
};
export const setWeatherError = (value) => {
  return {
    type: t.WEATHER_ERROR_HUNDLER,
    weather: value,
  };
};
