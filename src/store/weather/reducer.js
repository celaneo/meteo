import * as t from "./actionTypes";
const initialState = {
  weather: {},
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case t.WEATHER_LIST:
      return {
        ...state,
        weather: action.weather,
      };
    case t.WEATHER_ERROR_HUNDLER:
      return {
        ...state,
        weather: action.weather,
      };

    default:
      return state;
  }
};
export default reducer;
