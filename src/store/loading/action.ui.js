import * as t from "./actionTypes";

export const uiStartLoading = () => {
  return {
    type: t.UI_START_LOADING,
  };
};

export const uiStopLoading = () => {
  return {
    type: t.UI_STOP_LOADING,
  };
};
