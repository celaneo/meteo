import * as t from "./actionTypes";
const initialState = {
  isLoading: false,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case t.UI_START_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case t.UI_STOP_LOADING:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};
export default reducer;
