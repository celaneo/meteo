import { Home } from "@mui/icons-material";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
const mockedOnChange = jest.fn();

describe("input should be filled with title", () => {
  test("input should be empty initially", async () => {
    render(<Home title="" onChangeHadler={mockedOnChange} />);
    const input = screen.getByRole("textbox", { name: /search/i });
    expect(input).toHaveValue("");
  });

const renderComponent = (searchInputValue) => {
  const view = render(
    <Home title={searchInputValue} onChangeHadler={mockedOnChange} />
  );
  return view;
};
test("input value is the title props", async () => {
  renderComponent("test");
  const input = await screen.findByRole("input", { name: /search/i });
  await waitFor(() => expect(input).toHaveValue("test"));
})
