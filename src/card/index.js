import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import styled from "@emotion/styled";
import "./style.css";
const CardStyle = styled(Card)`
  cursor: pointer;
  width: 350px;
  margin: 20px;
  :hover {
    box-shadow: -2px 0px 26px -6px rgba(0, 0, 0, 0.81);
    -webkit-box-shadow: -2px 0px 26px -6px rgba(0, 0, 0, 0.81);
    -moz-box-shadow: -2px 0px 26px -6px rgba(0, 0, 0, 0.81);
  }
`;
export default function CardWheather({ data }) {
  const [cityInfo, setCityInfo] = useState(data);
  useEffect(() => {
    setCityInfo(data);
  }, [data]);
  return (
    <CardStyle>
      <CardHeader
        avatar={
          <Avatar
            className='avatar'
            aria-label='recipe'
            src={`http://openweathermap.org/img/w/${cityInfo.weather[0].icon}.png`}
          />
        }
        title={cityInfo.name + "," + cityInfo.sys.country}
        subheader={cityInfo.weather[0].main}
      />
      <div className='temp-container'>
        <h1>{`${cityInfo.main.temp}`}°C</h1>
        <img
          alt={cityInfo.name}
          src={`http://openweathermap.org/img/w/${cityInfo.weather[0].icon}.png`}
        />
      </div>

      <CardContent>
        <Typography variant='body2' color='text.secondary'>
          {"details"}
        </Typography>
        <ul>
          <li>
            ground level :<strong> {cityInfo.main.grnd_level} </strong>
          </li>
          <li>
            humidity : <strong> {cityInfo.main.humidity} </strong>
          </li>
          <li>
            pressure : <strong> {cityInfo.main.pressure} </strong>
          </li>
          <li>
            sea level : <strong>{cityInfo.main.sea_level} </strong>
          </li>
        </ul>
      </CardContent>
    </CardStyle>
  );
}
