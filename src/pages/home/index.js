import { CircularProgress, TextField } from "@mui/material";
import { Container } from "@mui/system";
import { useEffect, useState } from "react";
import CardWheather from "../../card";
import { useDispatch, useSelector } from "react-redux";
import { getWeather } from "../../store/weather/action.lang";
import "./style.css";

function Home() {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.isLoading.isLoading);
  const data = useSelector((state) => state.weather.weather);
  // const [city, setCity] = useState();
  const [dataWheather, setData] = useState(null);
  const [hundleError, setHundleError] = useState(false);

  useEffect(() => {
    if (data.name) {
      setData(data);
    }
    if (data.cod === "404") {
      setHundleError(true);
    } else {
      setHundleError(false);
    }
  }, [data.name]);

  const submitComment = async (city, event) => {
    setData(null);
    event.preventDefault();
    dispatch(getWeather({ city }));
  };

  return (
    <Container maxWidth='lg'>
      <div className='box'>
        <TextField
          sx={{ margin: 2 }}
          fullWidth
          name='search'
          id='outlined-basic'
          label='Rechercher une ville '
          variant='outlined'
          onKeyPress={(event) => {
            if (event.key === "Enter") {
              submitComment(event.target.value, event);
            }
          }}
        />
        {hundleError && <h1>{data.message} </h1>}
        {isLoading && <CircularProgress />}
        {dataWheather && !isLoading && <CardWheather data={dataWheather} />}
      </div>
    </Container>
  );
}
export default Home;
